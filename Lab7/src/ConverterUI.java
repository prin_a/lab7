import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ConverterUI is ui for unit converter.
 * @author Prin Angkunanuwat
 *
 */
public class ConverterUI extends JFrame
{
	// attributes for graphical components
	private JButton convertButton;
	private JTextField inputField1;
	private JTextField inputField2;
	private JComboBox comboLeft;
	private JComboBox comboRight;
	private JButton clearButton;
	private UnitConverter unitconverter;
	
	/**
	 * Construct and call initComponents.
	 * @param uc is unitConverter used to convert value.
	 */
	public ConverterUI( UnitConverter uc ) {
		this.unitconverter = uc;
		this.setTitle("Distance Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents( );
	}

	/**
	 * initialize components in the window
	 */
	private void initComponents() {

		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );

		convertButton = new JButton("Convert");
		inputField1 = new JTextField(10);
		inputField2 = new JTextField(10);
		comboLeft = new JComboBox<Unit>(unitconverter.getUnits());
		comboRight = new JComboBox<Unit>(unitconverter.getUnits());
		clearButton = new JButton("Clear");
		JLabel label = new JLabel("=");

		inputField2.setEditable(false);

		contents.add( inputField1 );
		contents.add( comboLeft );
		contents.add( label );
		contents.add( inputField2 );
		contents.add( comboRight );
		contents.add( convertButton );
		contents.add( clearButton );

		ActionListener listener = new ConvertButtonListener( );
		ActionListener clear = new ClearButtonListener( );

		clearButton.addActionListener( clear );
		convertButton.addActionListener( listener );
		inputField1.addActionListener( listener );

		this.pack(); 
		this.setVisible(true);
	}
	/**
	 * ActionListener get value from text1 and convert it 
	 * from unit in combobox1 to unit in combobox2.
	 * @author Prin Angkunanuwat
	 *
	 */
	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			String s = inputField1.getText().trim();
			if ( s.length() > 0 ) {
				try{
					double value = Double.valueOf( s );
					
					Unit unit1 =  (Unit) comboLeft.getSelectedItem();
					Unit unit2 =  (Unit) comboRight.getSelectedItem();
					
					double result = unitconverter.convert(value, unit1, unit2);
					inputField2.setText( result + "" );
					inputField1.setForeground(Color.black);
				} catch (NumberFormatException e){
					inputField1.setForeground(Color.red);
				}
			} 
		}
	} 
	/**
	 * Clear all text to empty String.
	 * @author Prin Angkunanuwat
	 *
	 */
	class ClearButtonListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			inputField1.setText("");
			inputField2.setText("");
		}
	}
}
